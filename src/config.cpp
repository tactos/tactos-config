#include "config.h"
#include <filesystem>
#include <fstream>
#include <glibmm/fileutils.h>
#include <iostream>

namespace YAML {

template <> struct convert<Gdk::RGBA> {
  static Node encode(const Gdk::RGBA &color) {
    Node node;
    node.push_back(color.get_red_u());
    node.push_back(color.get_green_u());
    node.push_back(color.get_blue_u());
    node.push_back(color.get_alpha_u());
    return node;
  }

  static bool decode(const Node &node, Gdk::RGBA &color) {
    if (!node.IsSequence() || node.size() < 3) {
      return false;
    }
    auto red = node[0].as<guint>(); // convert byte to guint
    auto green = node[1].as<guint>();
    auto blue = node[2].as<guint>();
    guint alpha = 255;
    if (node.size() == 4) {
      alpha = node[3].as<guint>();
    }
    color.set_rgba(red / 255.0, green / 255.0, blue / 255.0, alpha / 255.0);
    return true;
  }
};

Emitter &operator<<(Emitter &out, const Gdk::RGBA &c) {
  out << YAML::Flow;
  out << YAML::BeginSeq << (c.get_red_u() >> 8) << (c.get_green_u() >> 8) << (c.get_blue_u() >> 8) << (c.get_alpha_u() >> 8)
      << YAML::EndSeq;
  return out;
}
} // namespace YAML

using namespace Tactos::Config;

/**
 * @brief Oject to store configuration of tactos. Can either get data from file or DBus
 *
 * @param t_fileName Optional filename
 */
Configuration::Configuration(std::string t_fileName)
    : m_colorsStore(ColorListStore::create()), m_fileName(t_fileName), m_winSize(32, 32) {
  std::cout << "launching configuration" << std::endl;
  this->loadConfigIfFileExist(t_fileName);
}

/**
 * @brief save current configuration to file
 *
 * @param t_fileName p_t_fileName:...
 */
void Tactos::Config::Configuration::saveConfig(std::string t_fileName) {
  this->m_fileName = t_fileName;
  this->saveConfig();
}

/**
 * @brief save current configuration to previous configured file
 *
 */
void Tactos::Config::Configuration::saveConfig() {
  YAML::Emitter file;

  file << YAML::BeginMap;
  file << YAML::Key << "Window" << YAML::Value << YAML::BeginMap;
  file << YAML::Key << "width" << YAML::Value << this->m_winSize.first;
  file << YAML::Key << "height" << YAML::Value << this->m_winSize.second;
  file << YAML::EndMap;
  file << YAML::Key << "Colors" << YAML::Value << YAML::BeginSeq;
  for (guint i = 0; i < this->m_colorsStore->get_n_items(); ++i) {
    Glib::RefPtr<TactosColor> color = Glib::RefPtr<TactosColor>::cast_dynamic(this->m_colorsStore->get_object(i));
    file << YAML::BeginMap;
    if (color->name != "") {
      file << YAML::Key << "name" << YAML::Value << color->name;
    }
    if (color->chain) {
      file << YAML::Key << "color" << YAML::Value << color->min;
    } else {
      file << YAML::Key << "start" << YAML::Value << color->min;
      file << YAML::Key << "end" << YAML::Value << color->max;
    }
    file << YAML::Key << "pins" << YAML::Value << color->pins;
    file << YAML::Key << "active" << YAML::Value << color->active;
    file << YAML::Key << "speech" << YAML::BeginMap;
    file << YAML::Key << "sentence" << YAML::Value << color->TTS;
    file << YAML::Key << "trigger" << YAML::Value << color->speech_trigger;
    file << YAML::Key << "sound_icon" << YAML::Value << color->sound_icon;
    file << YAML::EndMap;
    file << YAML::EndMap;
  }
  file << YAML::EndSeq << YAML::EndMap;
  std::ofstream fout(this->m_fileName);
  fout << file.c_str();
}

/**
 * @brief load Configuration from file. Do nothing if file not found.
 *
 * @param t_fileName
 */
void Tactos::Config::Configuration::loadConfigIfFileExist(std::string t_fileName) {
  if (t_fileName != "" && std::filesystem::is_regular_file(t_fileName)) {
    this->m_fileName = t_fileName;
    this->loadConfig();
  }
}

/**
 * @brief load config from file
 */
void Configuration::loadConfig() {
  this->m_colorsStore->remove_all();
  TactosColor::color_index = 0;
  guint width = 32;
  guint height = 32;
  YAML::Node config;
  config = YAML::LoadFile(this->m_fileName);

  if (config["Window"]) {

    width = config["Window"]["width"].as<guint>();
    height = config["Window"]["height"].as<guint>();
  }
  this->m_winSize = std::make_pair(width, height);

  if (config["Colors"]) {
    YAML::Node colors = config["Colors"];
    try {
      for (auto color : colors) {
        Gdk::RGBA start, end;
        std::string name = "", speech = "";
        TactosColor::SPEECH_TRIGGER speech_trigger = TactosColor::SPEECH_TRIGGER::IMMEDIATE;
        bool active = true;
        bool pins = true;
        bool chain = false;
        bool sound_icon = false;
        if (color["color"]) {
          start = color["color"].as<Gdk::RGBA>();
          chain = true;
        } else {
          start = color["start"].as<Gdk::RGBA>();
          end = color["end"].as<Gdk::RGBA>();
        }
        if (color["name"]) {
          name = color["name"].as<std::string>();
        }
        if (color["pins"]) {
          pins = color["pins"].as<bool>();
        }
        if (color["active"]) {
          active = color["active"].as<bool>();
        }
        if (color["speech"]) {
          auto node_speech = color["speech"];
          if (node_speech.IsMap()) {
            speech = node_speech["sentence"].as<std::string>();
            if (node_speech["trigger"]) {
              uint8_t index_speech = node_speech["trigger"].as<int>();
              speech_trigger = static_cast<TactosColor::SPEECH_TRIGGER>(index_speech);
              if (node_speech["sound_icon"]) {
                sound_icon = node_speech["sound_icon"].as<bool>();
              }
            }
          } else {
            speech = color["speech"].as<std::string>();
          }
        }

        Glib::RefPtr<TactosColor> el;
        if (chain) {
          el = Glib::RefPtr<TactosColor>(new TactosColor(name, start, pins, speech, active, speech_trigger, sound_icon));
        } else {
          el =
              Glib::RefPtr<TactosColor>(new TactosColor(name, start, end, pins, speech, active, speech_trigger, sound_icon));
        }
        this->m_colorsStore->append(el);
      }
      this->updateWin(width, height);
    } catch (YAML::InvalidNode &e) {
      std::cerr << "ERROR : Invalid range color detection" << std::endl;
      throw;
    }
  }
}

bool Tactos::Config::Configuration::isNewFile() { return this->m_fileName.empty(); }

/**
 * @brief update current window size
 *
 * @param w new win
 * @param h new height
 */
void Configuration::updateWin(guint w, guint h) {
  this->m_winSize = std::make_pair(w, h);
  this->winUpdated.emit(w, h);
}

/**
 * @brief update height on capture window
 *
 * @param h new height
 */
void Configuration::updateHeight(guint h) { this->updateWin(std::get<0>(this->m_winSize), h); }

/**
 * @brief update width on capture window
 *
 * @param w new width
 */
void Configuration::updateWidth(guint w) { this->updateWin(w, std::get<1>(this->m_winSize)); }

std::pair<guint, guint> Tactos::Config::Configuration::getWinSize() { return m_winSize; }

bool Tactos::Config::Configuration::hasInvalidColors() {
  for (guint i = 0; i < this->m_colorsStore->get_n_items(); ++i) {
    Glib::RefPtr<TactosColor> color = Glib::RefPtr<TactosColor>::cast_dynamic(this->m_colorsStore->get_object(i));
    std::cout << color->chain << std::endl;
    if (!color->isValid()) {
      return true;
    }
  }
  return false;
}

void Tactos::Config::Configuration::fixInvalidColors() {
  for (guint i = 0; i < this->m_colorsStore->get_n_items(); ++i) {
    Glib::RefPtr<TactosColor> color = Glib::RefPtr<TactosColor>::cast_dynamic(this->m_colorsStore->get_object(i));
    if (!color->isValid()) {
      color->fixColor();
    }
  }
}
