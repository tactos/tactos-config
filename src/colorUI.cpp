#include "colorUI.h"
#include <functional>

#define ENTRY_ID "__custom__"

using namespace Tactos::Config;

const std::string ColorUI::INVALID_INTERVALLE_1 = "l'intervalle de la couleur ";
const std::string ColorUI::INVALID_INTERVALLE_2 = " n'est pas valide.";

const std::string ColorUI::SOUND_DIRECTORY = "/usr/share/sounds/sound-icons";

std::vector<std::string> listFileDirectory(std::filesystem::path path) {
  std::vector<std::string> result;
  if (std::filesystem::exists(path) and std::filesystem::is_directory(path)) {
    for (auto file : std::filesystem::directory_iterator(path)) {
      if (std::filesystem::is_regular_file(file) or std::filesystem::is_symlink(file)) {
        result.push_back(file.path().filename());
      }
    }
  }
  return result;
}

/**
 * @brief UI object to display a tactos Color
 *
 * @param color TactosColor to display
 * @param win Application that display it
 */
ColorUI::ColorUI(Glib::RefPtr<TactosColor> color, StackElementUI &win, std::filesystem::path sound_directory)
    : m_color(color), m_speech(true), m_name(), m_container(), m_active(), m_color_min(), m_color_max(), m_pins(),
      m_speech_trigger(), m_delete(), m_chain(color->chain) {
  this->add(m_container);
  m_container.pack_start(m_active_box, Gtk::PACK_SHRINK);
  m_active.set_valign(Gtk::ALIGN_CENTER);
  m_active_box.pack_start(m_active, Gtk::PACK_EXPAND_PADDING);
  m_container.pack_start(m_name, Gtk::PACK_SHRINK);
  m_container.pack_start(m_colors_box, Gtk::PACK_SHRINK);
  m_colors_box.pack_start(m_color_min_box, Gtk::PACK_SHRINK);
  m_color_min_box.pack_start(m_color_min, Gtk::PACK_EXPAND_PADDING);
  m_colors_box.pack_start(m_chain, Gtk::PACK_SHRINK);
  m_colors_box.pack_start(m_color_max_box, Gtk::PACK_SHRINK);
  m_color_max_box.pack_start(m_color_max, Gtk::PACK_EXPAND_PADDING);
  m_container.pack_start(m_pins_box, Gtk::PACK_SHRINK);
  m_pins_box.pack_start(m_pins, Gtk::PACK_EXPAND_PADDING);
  m_container.pack_start(m_speech, Gtk::PACK_EXPAND_WIDGET);
  m_container.pack_start(m_speech_trigger, Gtk::PACK_SHRINK);
  m_container.pack_start(m_delete, Gtk::PACK_SHRINK);
  m_delete.set_relief(Gtk::RELIEF_NONE);
  m_delete.set_image_from_icon_name("edit-delete-symbolic");

  // creating combobox
  m_speech_trigger.append("NOW", "Immédiat");
  m_speech_trigger.append("MOUSE_ALL", "Clic souris");
  m_speech_trigger.append("MOUSE_LEFT", "Clic gauche");
  m_speech_trigger.append("MOUSE_MIDDLE", "Clic milieu");
  m_speech_trigger.append("MIT_1", "Mit bouton 1");
  m_speech_trigger.append("MIT_2", "Mit bouton 2");
  m_speech_trigger.set_active_id(TactosColor::SPEECH_TRIGGER_NAME[color->speech_trigger]);

  this->m_speech.set_entry_text_column(12);
  for (auto sound_file : listFileDirectory(sound_directory)) {
    this->m_speech.append(sound_file, sound_file);
  }
  if (!color->sound_icon) {
    this->m_speech.prepend(ENTRY_ID, color->TTS);
  }
  m_speech.set_active_text(color->TTS);

  this->m_name.property_text() = color->name;
  this->m_pins.property_active() = color->pins;
  this->m_active.property_active() = color->active;
  this->m_color_min.property_rgba() = color->min;
  if (color->chain) {
    this->m_color_max.property_rgba() = color->min;
    this->m_color_max.set_sensitive(false);
  } else {
    this->m_color_max.property_rgba() = color->max;
  }

  this->m_color->sig_color.connect(sigc::mem_fun(*this, &ColorUI::update_color));

  m_name.signal_changed().connect(sigc::mem_fun(this, &ColorUI::on_name_updated));
  m_speech.signal_changed().connect(sigc::mem_fun(this, &ColorUI::on_speech_updated));
  m_pins.signal_toggled().connect(sigc::mem_fun(this, &ColorUI::on_pins_updated));
  m_active.property_active().signal_changed().connect(sigc::mem_fun(this, &ColorUI::on_active_updated));
  m_color_max.signal_color_set().connect(sigc::mem_fun(this, &ColorUI::on_color_max_updated));
  m_color_min.signal_color_set().connect(sigc::mem_fun(this, &ColorUI::on_color_min_updated));
  m_chain.signal_toggled().connect(sigc::mem_fun(this, &ColorUI::on_chain_updated));
  m_speech_trigger.signal_changed().connect(sigc::mem_fun(this, &ColorUI::on_speech_trigger_updated));

  this->liststore = win.get_config().m_colorsStore;
  this->notifications = sigc::mem_fun(win, &StackElementUI::send_notification_action);
  m_delete.signal_clicked().connect(sigc::mem_fun(this, &ColorUI::on_button_remove_clicked));
  show_all();
}

/**
 * @brief Synchronize tactosColor text field
 *
 */
void ColorUI::on_name_updated() { this->m_color->name = this->m_name.get_text(); }

/**
 * @brief Synchronize tactosColor active field
 *
 */
void ColorUI::on_active_updated() { this->m_color->active = this->m_active.get_active(); }

/**
 * @brief Synchronize tactosColor color_max field
 *
 */
void ColorUI::on_color_max_updated() {
  this->m_color->max = this->m_color_max.get_rgba();
  if (!this->m_color->isValid()) {
    std::string notif_text = ColorUI::INVALID_INTERVALLE_1 + this->m_color->name + ColorUI::INVALID_INTERVALLE_2;
    this->notifications(notif_text, "Corriger", sigc::mem_fun(this->m_color.get(), &TactosColor::fixColor));
  }
}

/**
 * @brief Synchronize tactosColor color min field
 * if link is active, update color max too
 *
 */
void ColorUI::on_color_min_updated() {
  this->m_color->min = this->m_color_min.get_rgba();
  if (!this->m_color_max.is_sensitive()) {
    this->m_color_max.set_rgba(this->m_color_min.get_rgba());
    this->on_color_max_updated();
  }
  if (!this->m_color->isValid()) {
    std::string notif_text = ColorUI::INVALID_INTERVALLE_1 + this->m_color->name + ColorUI::INVALID_INTERVALLE_2;
    this->notifications(notif_text, "Corriger", sigc::mem_fun(this->m_color.get(), &TactosColor::fixColor));
  }
}

/**
 * @brief Synchronize tactosColor pins field
 *
 */
void ColorUI::on_pins_updated() { this->m_color->pins = this->m_pins.get_active(); }

/**
 * @brief Synchronize tactosColor speech field
 *
 */
void ColorUI::on_speech_updated() {
  set_sound_icon();
  this->m_color->TTS = this->m_speech.get_entry_text();
}

void Tactos::Config::ColorUI::set_sound_icon() {
  Glib::ustring id = this->m_speech.get_active_id();
  if (id.empty() or id == ENTRY_ID) {
    this->m_color->sound_icon = false;
  } else {
    this->m_color->sound_icon = true;
  }
}

/**
 * @brief active or not color_max field
 *
 */
void ColorUI::on_chain_updated() {
  this->m_color->chain = this->m_chain.get_active();
  if (this->m_color->chain) {
    this->m_color_max.set_sensitive(false);
  } else {
    this->m_color_max.set_sensitive(true);
  }
}

/**
 * @brief  remove selected color. Furthermore, trigger a notification to undo the action
 *
 */
void ColorUI::on_button_remove_clicked() {
  std::string notif_text = "La couleur " + this->m_color->name + " vient dêtre supprimée";
  this->notifications(notif_text, "Annuler", sigc::mem_fun(liststore.get(), &ColorListStore::undo));
  liststore->remove_item(*this->m_color.get());
}

void ColorUI::on_speech_trigger_updated() {
  this->m_color->speech_trigger = static_cast<TactosColor::SPEECH_TRIGGER>(m_speech_trigger.get_active_row_number());
}

/**
 * @brief update ui  colors component
 *
 * @param start minimum value of the range
 * @param end maximum value of the range
 */
void ColorUI::update_color(Gdk::RGBA start, Gdk::RGBA end) {
  this->m_color_min.property_rgba() = start;
  this->m_color_max.property_rgba() = end;
}
