#include "stackElementUI.h"
#include "colorUI.h"
#include <gtkmm/scrolledwindow.h>
#include <gtkmm/viewport.h>

using namespace Tactos::Config;

Tactos::Config::StackElementUI::StackElementUI(std::string filename, Tactos::Config::ApplicationWin &win)
    : config(filename) {
  this->notifications = sigc::mem_fun(win, &ApplicationWin::send_notification_action);

  auto scroll = Gtk::make_managed<Gtk::ScrolledWindow>();
  scroll->add(colors_list);
  this->set_orientation(Gtk::ORIENTATION_VERTICAL);
  this->pack_start(*scroll, Gtk::PACK_EXPAND_WIDGET);

  colors_list.bind_list_store(Glib::RefPtr<Gio::ListStore<TactosColor>>::cast_dynamic(config.m_colorsStore),
                              sigc::mem_fun(win, &ApplicationWin::on_create_widget));
  show_all();
}

Tactos::Config::Configuration &Tactos::Config::StackElementUI::get_config() { return this->config; }

/**
 * @brief call ApplicationWin function
 *
 * @param text
 * @param button_text
 * @param callback
 */
void Tactos::Config::StackElementUI::send_notification_action(std::string text, std::string button_text,
                                                              std::function<void()> callback) {
  this->notifications(text, button_text, callback);
}
