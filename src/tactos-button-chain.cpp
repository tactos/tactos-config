#include "tactos-button-chain.h"
#include <gdkmm/pixbuf.h>
#include <glibmm/binding.h>

using namespace Tactos::Config;

/**
 * @brief button inheritance to add picture with linked/not linked
 *
 * @param chain initial value of the button
 */
ChainButton::ChainButton(bool chain)
    : Gtk::CheckButton(), chain_close(Gdk::Pixbuf::create_from_resource("/color/chain-close.svg")),
      chain_open(Gdk::Pixbuf::create_from_resource("/color/chain-open.svg")) {

  this->set_mode(false);
  this->set_relief(Gtk::RELIEF_NONE);
  this->set_active(chain);
  if (chain) {
    this->set_image(chain_close);
  } else {
    this->set_image(chain_open);
  }
  this->signal_toggled().connect(sigc::mem_fun(this, &ChainButton::toggle_icon));
}

/**
 * @brief callback triggered when button is pressed. Change picture.
 *
 */
void ChainButton::toggle_icon() {
  if (this->get_active()) {
    this->set_image(chain_close);
  } else {
    this->set_image(chain_open);
  }
}
