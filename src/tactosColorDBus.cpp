#include "tactosColorDBus.h"

typedef std::tuple<uint16_t, std::tuple<guint32, guint32>, bool, guchar, Glib::ustring, Glib::ustring, bool> DBUS_Color;
using namespace Tactos::Config;

/**
 * @brief convert uint32_t (RRGGBBAA) to Gdk::RGBA
 *
 * @param c color uint32_t (RRGGBBAA)
 * @return RGBA value
 */
Gdk::RGBA int2RGBA(uint32_t c) {
  Gdk::RGBA color;
  color.set_rgba(((c >> 24) & 0xFF) / 255.0, ((c >> 16) & 0xFF) / 255.0, ((c >> 8) & 0xFF) / 255.0, (c & 0xFF) / 255.0);
  return color;
}

/**
 * @brief convert RGBA to uint32_t (RRGGBBAA)
 *
 * @param c Gdk::RGBA
 * @return uint32_t(RRGGBBAA)
 */
uint32_t RGBA2int(Gdk::RGBA c) {
  auto result = static_cast<uint32_t>(c.get_alpha_u() >> 8) + (static_cast<uint32_t>(c.get_blue_u() >> 8) << 8) +
                (static_cast<uint32_t>(c.get_green_u() >> 8) << 16) + (static_cast<uint32_t>(c.get_red_u() >> 8) << 24);
  return result;
}

/**
 * @brief DBUS Proxy
 *
 */
TactosColorDBus::TactosColorDBus() : connected(false) {
  proxy = {};
  Glib::init();
  Gio::init();
  connect();
}

/**
 * @brief try to connect to DBus proxy at "org.tactos.color. SHould not be used, use tryCOnnect instead
 *
 * @return True if success Famse otherwise.
 */
bool TactosColorDBus::connect() {
  proxy = org::tactos::colorProxy::createForBus_sync(Gio::DBus::BUS_TYPE_SESSION, Gio::DBus::PROXY_FLAGS_NONE,
                                                     "org.tactos.color", "/org/tactos/color", Gio::Cancellable::create());
  connected = true;
  std::cout << "launching DBus service" << std::endl;
  return true;
}

/**
 * @brief check wether proxy is connected or not
 *
 * @return True if connectes
 */
bool TactosColorDBus::isConnected() { return connected; }

/**
 * @brief check if proxy is connected. Try to connect if not
 *
 * @return True if connected
 */
bool TactosColorDBus::tryConnect() {
  if (!isConnected()) {
    return connect();
  }
  return true;
}

/**
 * @brief get current tactos core color configuration
 *
 * @return std::vector< Tactos::TactosColor >
 */
std::optional<std::vector<Glib::RefPtr<TactosColor>>> TactosColorDBus::getColors() {
  std::cout << "get colors" << std::endl;
  std::vector<Glib::RefPtr<TactosColor>> result;
  try {
    tryConnect();
    auto list = proxy->get()->getColors_sync();
    for (auto color : list) {
      uint16_t index = std::get<0>(color);
      Gdk::RGBA min = int2RGBA(std::get<0>(std::get<1>(color)));
      Gdk::RGBA max = int2RGBA(std::get<1>(std::get<1>(color)));
      bool pins = std::get<2>(color);
      TactosColor::SPEECH_TRIGGER speech_trigger = (TactosColor::SPEECH_TRIGGER)std::get<3>(color);
      std::string speech = std::get<4>(color);
      std::string name = std::get<5>(color);
      bool sound_icon = std::get<6>(color);
      if (min == max) {
        result.push_back(TactosColor::create(name, min, pins, speech, true, speech_trigger, sound_icon, index));
      } else {
        result.push_back(TactosColor::create(name, min, max, pins, speech, true, speech_trigger, sound_icon, index));
      }
    }
  } catch (Gio::DBus::Error &) {
    std::cout << "DBUS error" << std::endl;
    return {};
  }
  return result;
}

/**
 * @brief update tactos_core_color configuration
 *
 * @param colors list of new colors
 * @return True if success
 */
bool TactosColorDBus::setColors(std::vector<Glib::RefPtr<TactosColor>> colors) {
  tryConnect();
  std::vector<DBUS_Color> DBusList;
  for (auto color : colors) {
    if (color->active) {
      auto range = std::make_pair(RGBA2int(color->min), RGBA2int(color->chain ? color->min : color->max));
      DBusList.emplace_back(std::make_tuple(color->index, range, color->pins, color->speech_trigger, color->TTS, color->name,
                                            color->sound_icon));
    }
  }
  try {
    proxy->get()->loadConfigList_sync(DBusList);
  } catch (Gio::DBus::Error &) {
    std::cerr << "update failed" << std::endl;
    return false;
  }
  return true;
}

/**
 * @brief update Capture WIndow
 *
 * @param winSize p_winSize:...
 * @return bool True if success, bool otherwise
 */
bool TactosColorDBus::setWinSize(std::pair<guint, guint> winSize) {
  tryConnect();
  try {
    std::pair<guchar, guchar> value = static_cast<std::pair<guchar, guchar>>(winSize);
    proxy->get()->detectionWin_set_sync(value);
  } catch (Gio::DBus::Error &) {
    return false;
  }
  return true;
}

/**
 * @brief get current capture window
 *
 * @return std::pair< guint, guint >
 */
std::pair<guint, guint> TactosColorDBus::getWinSize() {
  tryConnect();
  auto winSize = proxy->get()->detectionWin_get();
  return std::make_pair((guint)std::get<0>(winSize), (guint)std::get<1>(winSize));
}
