#include "applicationwin.h"
#include "colorUI.h"
#include <glibmm/binding.h>
#include <gtkmm/filechoosernative.h>
#include <gtkmm/messagedialog.h>
#include <iostream>

#define NEW_FILE_TITLE "Nouveau document"
#define TACTOS_CORE_TITLE "Configuration courante"
#define NEW_FILE_NAME "__new_file__"
#define TACTOS_CORE_NAME "__tactos_core__"

typedef Gtk::FileChooserNative FileChooser_t;
using namespace Tactos::Config;

ApplicationWin::ApplicationWin(BaseObjectType *cobject, const Glib::RefPtr<Gtk::Builder> &refGlade)
    : Gtk::ApplicationWindow(cobject), m_ui(refGlade), m_DBus_color() {
  load_widgets_and_actions();
  notification_init();
  open_file("", NEW_FILE_NAME, NEW_FILE_TITLE);
  show_all();
}

void Tactos::Config::ApplicationWin::load_widgets_and_actions() {
  if (m_ui) {
    m_grp_colors = Glib::RefPtr<Gtk::SizeGroup>::cast_dynamic(m_ui->get_object("colColors"));
    m_grp_min = Glib::RefPtr<Gtk::SizeGroup>::cast_dynamic(m_ui->get_object("colMin"));
    m_grp_min = Glib::RefPtr<Gtk::SizeGroup>::cast_dynamic(m_ui->get_object("colMin"));
    m_grp_max = Glib::RefPtr<Gtk::SizeGroup>::cast_dynamic(m_ui->get_object("colMax"));
    m_grp_active = Glib::RefPtr<Gtk::SizeGroup>::cast_dynamic(m_ui->get_object("colActivated"));
    m_grp_name = Glib::RefPtr<Gtk::SizeGroup>::cast_dynamic(m_ui->get_object("colName"));
    m_grp_pins = Glib::RefPtr<Gtk::SizeGroup>::cast_dynamic(m_ui->get_object("colPins"));
    m_grp_speech = Glib::RefPtr<Gtk::SizeGroup>::cast_dynamic(m_ui->get_object("colTTS"));

    m_tabs = Glib::RefPtr<Gtk::Stack>::cast_dynamic(m_ui->get_object("files_stack"));

    this->add_action("save_as", sigc::bind(sigc::mem_fun(*this, &ApplicationWin::chek_invalid_color), true));
    this->add_action("save", sigc::bind(sigc::mem_fun(*this, &ApplicationWin::chek_invalid_color), false));
    this->add_action("update_save", sigc::mem_fun(*this, &ApplicationWin::on_button_update_save_clicked));
    this->add_action("add_color", sigc::mem_fun(*this, &ApplicationWin::on_button_add_clicked));
    this->add_action("open",
                     sigc::bind(sigc::mem_fun(*this, &ApplicationWin::dialog_file_chooser), Gtk::FILE_CHOOSER_ACTION_OPEN));
    this->add_action("add_color", sigc::mem_fun(*this, &ApplicationWin::on_button_add_clicked));
    this->add_action("new_conf", sigc::mem_fun(*this, &ApplicationWin::on_button_new_clicked));
    this->add_action("actualize", sigc::mem_fun(*this, &ApplicationWin::on_button_actualize_clicked));
    this->add_action("notification.close", sigc::mem_fun(*this, &ApplicationWin::on_button_notification_close_clicked));
    this->add_action("tab.close", sigc::mem_fun(*this, &ApplicationWin::on_button_tab_close_clicked));
  } else {
    throw std::invalid_argument("ui file not found");
  }
}

void Tactos::Config::ApplicationWin::notification_init() {
  m_notifications_button = Glib::RefPtr<Gtk::Button>::cast_dynamic(m_ui->get_object("notifications_button"));
  m_notifications_label = Glib::RefPtr<Gtk::Label>::cast_dynamic(m_ui->get_object("notifications_label"));
  m_notifications = Glib::RefPtr<Gtk::Revealer>::cast_dynamic(m_ui->get_object("notifications"));
  m_notifications_button->set_label("");
  m_notifications_button->hide();
  m_notifications->set_reveal_child(false);
}

Gtk::Widget *ApplicationWin::on_create_widget(const Glib::RefPtr<TactosColor> &item) {
  if (!item) {
    std::cout << "on_create_widget(): item is empty" << std::endl;
    return nullptr;
  }

  auto current_config = static_cast<StackElementUI *>(this->m_tabs->get_visible_child());
  auto row = Gtk::manage(new ColorUI(item, *current_config));

  this->m_grp_min->add_widget(row->m_color_min_box);
  this->m_grp_max->add_widget(row->m_color_max_box);
  this->m_grp_active->add_widget(row->m_active_box);
  this->m_grp_name->add_widget(row->m_name);
  this->m_grp_pins->add_widget(row->m_pins_box);
  this->m_grp_speech->add_widget(row->m_speech);
  this->m_grp_colors->add_widget(row->m_colors_box);

  return row;
}

/**
 * @brief open file chooser dialog and call save or open method accordingly to action
 *
 * @param action open or save
 */
void ApplicationWin::dialog_file_chooser(Gtk::FileChooserAction action) {
  const bool open = (action == Gtk::FILE_CHOOSER_ACTION_OPEN);
  auto dialog = FileChooser_t::create("Please choose a file", *this, action);
  // Show the dialog and wait for a user response:
  const int result = dialog->run();

  // Handle the response:
  if (result == Gtk::RESPONSE_ACCEPT) {
    std::string filename = dialog->get_filename();
    if (open) {
      std::string stem = std::filesystem::path(filename).stem();
      open_file(filename, stem, stem);
    } else {
      save_as(filename);
    }
  }
}

void Tactos::Config::ApplicationWin::chek_invalid_color(bool t_save_as) {
  bool doSave = true;
  auto current_tab = static_cast<Tactos::Config::StackElementUI *>(this->m_tabs->get_visible_child());
  auto config = current_tab->get_config();
  if (config.hasInvalidColors()) {
    doSave = ask_invalid_color(config);
  }

  if (doSave) {
    if (t_save_as or config.isNewFile()) {
      this->dialog_file_chooser(Gtk::FILE_CHOOSER_ACTION_SAVE);
    } else {
      save();
    }
  }
}

/**
 * @brief ask about saving with invalid color and fix it if chosen.
 * @return True if should save, False otherwise
 */
bool Tactos::Config::ApplicationWin::ask_invalid_color(Tactos::Config::Configuration config) {
  Gtk::MessageDialog dialog(*this, "Certaine couleurs sont invalide. Corriger les couleurs ?", false, Gtk::MESSAGE_QUESTION,
                            Gtk::BUTTONS_NONE);
  dialog.add_button("Annuler", Gtk::RESPONSE_CANCEL);
  dialog.add_button("Continuer", Gtk::RESPONSE_REJECT);
  dialog.add_button("Corriger", Gtk::RESPONSE_ACCEPT);
  const int result = dialog.run();

  switch (result) {
  case Gtk::RESPONSE_ACCEPT:
    config.fixInvalidColors();
    break;
  case Gtk::RESPONSE_REJECT:
    break;
  default:
    return false;
  }
  return true;
}

/**
 * @brief open new file and display it
 *
 * @param filename
 * @param stack_name
 * @param stack_title title displayed on the stack switcher
 */
void Tactos::Config::ApplicationWin::open_file(std::string filename, std::string stack_name, std::string stack_title) {
  auto tab = static_cast<Tactos::Config::StackElementUI *>(this->m_tabs->get_child_by_name(stack_name));
  if (tab == nullptr) {
    auto tab_ui = Gtk::make_managed<StackElementUI>(filename, *this);
    this->m_tabs->add(*tab_ui, stack_name, stack_title);
  } else {
    tab->get_config().loadConfigIfFileExist(filename);
  }
  this->m_tabs->set_visible_child(stack_name);
}

void ApplicationWin::save_as(std::string filename) {
  auto current_tab = static_cast<Tactos::Config::StackElementUI *>(this->m_tabs->get_visible_child());
  std::string stem = std::filesystem::path(filename).stem();
  current_tab->set_name(stem);
  m_tabs->child_property_name(*current_tab).set_value(stem);
  m_tabs->child_property_title(*current_tab).set_value(stem);
  current_tab->get_config().saveConfig(filename);
  this->send_notification("fichier " + filename + " enregistré.");
}

void ApplicationWin::save() {
  auto current_tab = static_cast<Tactos::Config::StackElementUI *>(this->m_tabs->get_visible_child());
  current_tab->get_config().saveConfig();
  this->send_notification("fichier  enregistré.");
}

void ApplicationWin::on_button_add_clicked() {
  std::cout << "adding new color" << std::endl;
  Glib::RefPtr<TactosColor> el(new TactosColor());
  auto config = static_cast<Tactos::Config::StackElementUI *>(this->m_tabs->get_visible_child());
  config->get_config().m_colorsStore->append(el);
}

void Tactos::Config::ApplicationWin::on_button_new_clicked() { open_file("", NEW_FILE_NAME, NEW_FILE_TITLE); }

void ApplicationWin::on_button_actualize_clicked() {
  auto tactos_core_tab = static_cast<Tactos::Config::StackElementUI *>(this->m_tabs->get_child_by_name(TACTOS_CORE_NAME));
  if (tactos_core_tab == nullptr) {
    open_file("", TACTOS_CORE_NAME, TACTOS_CORE_TITLE);
    tactos_core_tab = static_cast<Tactos::Config::StackElementUI *>(this->m_tabs->get_child_by_name(TACTOS_CORE_NAME));
  }
  auto colors = this->m_DBus_color.getColors();
  if (colors) {
    tactos_core_tab->get_config().m_colorsStore->setColors(*colors);
    this->m_tabs->set_visible_child(TACTOS_CORE_NAME);
  } else {
    this->send_notification("connection à tactos impossible, la configuration n'a pas été chargée");
  }
}

void ApplicationWin::on_button_update_save_clicked() {
  std::cout << "updating tactos core" << std::endl;
  auto current_tab = static_cast<Tactos::Config::StackElementUI *>(this->m_tabs->get_visible_child());
  auto config = current_tab->get_config();
  bool colors_set = this->m_DBus_color.setColors(config.m_colorsStore->getColors());
  if (!colors_set) {
    this->send_notification("connection à tactos impossible, la configuration n'a pas été chargée");
  }
}

void Tactos::Config::ApplicationWin::on_button_tab_close_clicked() {
  auto current_tab = static_cast<Tactos::Config::StackElementUI *>(this->m_tabs->get_visible_child());
  this->m_tabs->remove(*current_tab);
  if (this->m_tabs->get_children().empty()) {
    this->hide();
  }
}

void ApplicationWin::on_button_notification_close_clicked() { close_notification(); }

bool ApplicationWin::close_notification() {
  this->m_notifications_button->hide();
  this->m_notifications->set_reveal_child(false);
  return true;
}

/**
 * @brief display text as notification and button to call callback
 *
 * @param text
 * @param button_text
 * @param callback function called when button is pressed
 */
void Tactos::Config::ApplicationWin::send_notification_action(std::string text, std::string button_text,
                                                              std::function<void()> callback) {
  this->m_notifications_button->set_label(button_text);
  this->m_notifications_button->show();
  this->add_action("notification.action", callback);
  this->send_notification(text);
}

/**
 * @brief display text as notification with default timeout
 *
 * @param text
 */
void Tactos::Config::ApplicationWin::send_notification(std::string text) {
  notification_timeout.disconnect();
  notification_timeout =
      Glib::signal_timeout().connect(sigc::mem_fun(*this, &ApplicationWin::close_notification), timeout_notif_ms);
  this->m_notifications_label->set_text(text);
  this->m_notifications->set_reveal_child();
}
