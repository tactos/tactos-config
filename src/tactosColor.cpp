#include "tactosColor.h"

using namespace Tactos::Config;

const std::string TactosColor::SPEECH_TRIGGER_NAME[] = {"NOW", "MOUSE_ALL", "MOUSE_LEFT", "MOUSE_MIDDLE", "MIT_1", "MIT_2"};
uint16_t TactosColor::color_index = 0;

TactosColor::TactosColor(std::string t_name, Gdk::RGBA t_color, bool t_pins, std::string t_TTS, bool t_active,
                         Tactos::Config::TactosColor::SPEECH_TRIGGER t_speech_trigger, bool t_sound_icon, uint16_t t_index)
    : Glib::ObjectBase(typeid(TactosColor)), name(t_name), min(t_color), max(t_color), pins(t_pins), TTS(t_TTS),
      active(t_active), chain(true), speech_trigger(t_speech_trigger), index(t_index), sound_icon(t_sound_icon) {
  TactosColor::color_index = index + 1;
}

TactosColor::TactosColor(std::string t_name, Gdk::RGBA t_min, Gdk::RGBA t_max, bool t_pins, std::string t_TTS, bool t_active,
                         Tactos::Config::TactosColor::SPEECH_TRIGGER t_speech_trigger, bool t_sound_icon, uint16_t t_index)
    : Glib::ObjectBase(typeid(TactosColor)), name(t_name), min(t_min), max(t_max), pins(t_pins), TTS(t_TTS),
      active(t_active), chain(false), speech_trigger(t_speech_trigger), index(t_index), sound_icon(t_sound_icon) {
  TactosColor::color_index = index + 1;
}

/**
 * @brief create new tactosColor RefPtr with range color detection
 *
 * @param t_name p_t_name:...
 * @param t_min p_t_min:...
 * @param t_max p_t_max:...
 * @param t_pins p_t_pins:...
 * @param t_TTS p_t_TTS:...
 * @param t_active p_t_active:...
 * @return Glib::RefPtr< Tactos::TactosColor >
 */
Glib::RefPtr<TactosColor> TactosColor::create(std::string t_name, Gdk::RGBA t_min, Gdk::RGBA t_max, bool t_pins,
                                              std::string t_TTS, bool t_active, SPEECH_TRIGGER t_speech_trigger,
                                              bool t_sound_icon, uint16_t t_index) {
  return Glib::RefPtr<TactosColor>(
      new TactosColor(t_name, t_min, t_max, t_pins, t_TTS, t_active, t_speech_trigger, t_sound_icon, t_index));
}

/**
 * @brief create new tactosColor RefPtr with one color selection
 *
 * @param t_name p_t_name:...
 * @param t_min p_t_min:...
 * @param t_pins p_t_pins:...
 * @param t_TTS p_t_TTS:...
 * @param t_active p_t_active:...
 * @return Glib::RefPtr< Tactos::TactosColor >
 */
Glib::RefPtr<TactosColor> TactosColor::create(std::string t_name, Gdk::RGBA t_min, bool t_pins, std::string t_TTS,
                                              bool t_active, SPEECH_TRIGGER t_speech_trigger, bool t_sound_icon,
                                              uint16_t t_index) {
  return Glib::RefPtr<TactosColor>(
      new TactosColor(t_name, t_min, t_pins, t_TTS, t_active, t_speech_trigger, t_sound_icon, t_index));
}

/**
 * @brief check if the color is valid. it means that some of color composant are lesser in max than in min
 *
 * @return bool
 */
bool Tactos::Config::TactosColor::isValid() {
  if (this->chain) {
    return true;
  }
  return this->max.get_red() >= this->min.get_red() and this->max.get_green() >= this->min.get_green() and
         this->max.get_blue() >= this->min.get_blue();
}

/**
 * @brief create valid range from min and max colors
 *
 */
void Tactos::Config::TactosColor::fixColor() {
  auto min_r = std::min(this->max.get_red(), this->min.get_red());
  auto min_g = std::min(this->max.get_green(), this->min.get_green());
  auto min_b = std::min(this->max.get_blue(), this->min.get_blue());
  auto max_r = std::max(this->max.get_red(), this->min.get_red());
  auto max_g = std::max(this->max.get_green(), this->min.get_green());
  auto max_b = std::max(this->max.get_blue(), this->min.get_blue());
  this->min.set_rgba(min_r, min_g, min_b);
  this->max.set_rgba(max_r, max_g, max_b);
  this->sig_color.emit(this->min, this->max);
}

/**
 * @brief remove specific item from the store
 *
 * @param color item to remove
 */
void ColorListStore::remove_item(const TactosColor &color) {
  size_t i = 0;
  for (; i < this->get_n_items(); ++i) {
    if (this->get_item(i).get() == &color) {
      this->lastDel = this->get_item(i);
      this->remove(i);
      TactosColor::color_index--;
      break;
    }
  }
  for (; i < this->get_n_items(); ++i) {
    this->get_item(i).get()->index--;
  }
}

/**
 * @brief create new store
 *
 * @return Glib::RefPtr< Tactos::ColorListStore >
 */
Glib::RefPtr<ColorListStore> ColorListStore::create() {
  std::cout << "creating store" << std::endl;
  return Glib::RefPtr<ColorListStore>(new ColorListStore());
}

/**
 * @brief get all colors from the store
 *
 * @return std::vector< Glib::RefPtr< Tactos::TactosColor > >
 */
std::vector<Glib::RefPtr<TactosColor>> ColorListStore::getColors() {
  std::vector<Glib::RefPtr<TactosColor>> colors;
  colors.reserve(this->get_n_items());
  for (size_t i = 0; i < this->get_n_items(); ++i) {
    colors.push_back(this->get_item(i));
  }
  return colors;
}

/**
 * @brief fill store with vector of colors
 *
 * @param colors p_colors:...
 */
void ColorListStore::setColors(std::vector<Glib::RefPtr<TactosColor>> colors) {

  this->splice(0, this->get_n_items(), colors);
  this->sort([](Glib::RefPtr<const TactosColor> item1, Glib::RefPtr<const TactosColor> item2) {
    if (item1->index < item2->index) {
      return -1;
    } else if (item1->index > item2->index) {
      return 1;
    } else {
      return 0;
    }
  });
}

/**
 * @brief reset last removed item in the store
 *
 */
void ColorListStore::undo() {
  if (this->lastDel) {
    this->lastDel->index = TactosColor::color_index++;
    this->append(this->lastDel);
    this->lastDel.reset();
  }
}
