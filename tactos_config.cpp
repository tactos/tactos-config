#include "applicationwin.h"
#include <filesystem>
#include <gtkmm-3.0/gtkmm.h>
#include <iostream>

int main() {
  auto app = Gtk::Application::create("org.tactos.config");

  // Load the Glade file and instantiate its widgets:
  auto refBuilder = Gtk::Builder::create_from_resource("/interface/interface.glade");

  try {
    //     refBuilder->add_from_file(filePath);
    Tactos::Config::ApplicationWin *pDialog;
    refBuilder->get_widget_derived("tactos_configuration", pDialog);
    app->run(*pDialog);
  } catch (const Glib::FileError &ex) {
    std::cerr << "FileError: " << ex.what() << std::endl;
    return 1;
  } catch (const Glib::MarkupError &ex) {
    std::cerr << "MarkupError: " << ex.what() << std::endl;
    return 1;
  } catch (const Gtk::BuilderError &ex) {
    std::cerr << "BuilderError: " << ex.what() << std::endl;
    return 1;
  }
  return 0;
}
