#pragma once

#include <gtkmm/applicationwindow.h>
#include <gtkmm/builder.h>
#include <gtkmm/button.h>
#include <gtkmm/filechooserdialog.h>
#include <gtkmm/listbox.h>
#include <gtkmm/revealer.h>
#include <gtkmm/sizegroup.h>
#include <gtkmm/spinbutton.h>
#include <gtkmm/stack.h>

#include <memory>

#include "config.h"
#include "tactosColorDBus.h"

namespace Tactos::Config {

class ApplicationWin : public Gtk::ApplicationWindow {
public:
  ApplicationWin(BaseObjectType *cobject, const Glib::RefPtr<Gtk::Builder> &refGlade);

  unsigned int timeout_notif_ms = 5000; ///< timeout for notification  in ms.
  void send_notification(std::string text);
  void send_notification_action(std::string text, std::string button_text, std::function<void()> callback);
  Gtk::Widget *on_create_widget(const Glib::RefPtr<TactosColor> &item);

private:
  void notification_init();
  void load_widgets_and_actions();
  void chek_invalid_color(bool t_save_as = true);
  bool ask_invalid_color(Tactos::Config::Configuration config);
  void on_button_add_clicked();
  void on_button_new_clicked();
  void on_button_actualize_clicked();
  void on_button_update_save_clicked();
  void on_button_tab_close_clicked();
  void on_button_notification_close_clicked();
  bool close_notification();
  void dialog_file_chooser(Gtk::FileChooserAction action);
  void open_file(std::string filename, std::string stack_name, std::string stack_title);
  void save_as(std::string filename);
  void save();
  Glib::RefPtr<Gtk::Builder> m_ui;
  Glib::RefPtr<Gtk::Stack> m_tabs;

  Glib::RefPtr<Gtk::Button> m_notifications_button;
  Glib::RefPtr<Gtk::Label> m_notifications_label;
  Glib::RefPtr<Gtk::Revealer> m_notifications;
  sigc::connection notification_timeout;

  Glib::RefPtr<Gtk::SizeGroup> m_grp_colors;
  Glib::RefPtr<Gtk::SizeGroup> m_grp_min;
  Glib::RefPtr<Gtk::SizeGroup> m_grp_max;
  Glib::RefPtr<Gtk::SizeGroup> m_grp_active;
  Glib::RefPtr<Gtk::SizeGroup> m_grp_name;
  Glib::RefPtr<Gtk::SizeGroup> m_grp_pins;
  Glib::RefPtr<Gtk::SizeGroup> m_grp_speech;

  TactosColorDBus m_DBus_color;
};

} // namespace Tactos::Config
