#pragma once

#include "applicationwin.h"
#include "config.h"
#include <gtkmm/box.h>
#include <gtkmm/button.h>
#include <gtkmm/label.h>
#include <gtkmm/listbox.h>
#include <gtkmm/sizegroup.h>
#include <gtkmm/spinbutton.h>

namespace Tactos::Config {

class StackElementUI : public Gtk::Box {
public:
  StackElementUI(std::string filename, ApplicationWin &win);
  void send_notification_action(std::string text, std::string button_text, std::function<void()> callback);

  Configuration &get_config();

private:
  std::function<void(std::string, std::string, std::function<void()>)> notifications;

  Configuration config;
  Gtk::ListBox colors_list;
};

} // namespace Tactos::Config
