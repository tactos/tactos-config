#pragma once

#include <filesystem>

#include <gtkmm/box.h>
#include <gtkmm/builder.h>
#include <gtkmm/button.h>
#include <gtkmm/checkbutton.h>
#include <gtkmm/colorbutton.h>
#include <gtkmm/comboboxtext.h>
#include <gtkmm/entry.h>
#include <gtkmm/listboxrow.h>
#include <gtkmm/switch.h>

#include "stackElementUI.h"
#include "tactos-button-chain.h"
#include "tactosColor.h"

namespace Tactos::Config {

class ColorUI : public Gtk::ListBoxRow {
  static const std::string SOUND_DIRECTORY;

public:
  ColorUI(Glib::RefPtr<TactosColor> color, StackElementUI &win, std::filesystem::path sound_directoy = SOUND_DIRECTORY);
  Glib::RefPtr<TactosColor> m_color;
  Gtk::Box m_pins_box;
  Gtk::ComboBoxText m_speech;
  Gtk::Entry m_name;
  Gtk::Box m_colors_box;
  Gtk::Box m_active_box;
  Gtk::Box m_color_min_box;
  Gtk::Box m_color_max_box;

private:
  Gtk::Box m_container;
  Gtk::Switch m_active;
  Gtk::ColorButton m_color_min;
  Gtk::ColorButton m_color_max;
  Gtk::CheckButton m_pins;
  Gtk::ComboBoxText m_speech_trigger;
  Gtk::Button m_delete;
  ChainButton m_chain;
  std::function<void(std::string, std::string, std::function<void()>)> notifications;
  Glib::RefPtr<ColorListStore> liststore;
  void update_color(Gdk::RGBA start, Gdk::RGBA end);
  void on_chain_updated();
  void on_active_updated();
  void on_name_updated();
  void on_speech_updated();
  void on_pins_updated();
  void on_color_min_updated();
  void on_color_max_updated();
  void on_button_remove_clicked();
  void on_speech_trigger_updated();
  void set_sound_icon();
  static const std::string INVALID_INTERVALLE_1;
  static const std::string INVALID_INTERVALLE_2;
};

} // namespace Tactos::Config
