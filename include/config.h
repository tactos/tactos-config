#pragma once
#include "tactosColor.h"
#include <giomm/liststore.h>
#include <glibmm/refptr.h>
#include <yaml-cpp/yaml.h>

namespace Tactos::Config {
class Configuration {

public:
  Configuration(std::string t_fileName = "");
  void saveConfig();
  void saveConfig(std::string t_fileName);
  void loadConfigIfFileExist(std::string t_fileName);
  Glib::RefPtr<ColorListStore> m_colorsStore; ///< list of tactosColors
  void updateWidth(guint w);
  void updateHeight(guint h);
  void updateWin(guint w, guint h);
  sigc::signal<void, guint, guint> winUpdated; ///< send signal  when winsize is updated
  std::pair<guint, guint> getWinSize();
  bool hasInvalidColors();
  void fixInvalidColors();
  bool isNewFile();

private:
  void loadConfig();
  std::string m_fileName;
  std::pair<guint, guint> m_winSize;
};
} // namespace Tactos::Config
