#pragma once
#include <gdkmm/rgba.h>
#include <giomm/liststore.h>
#include <glibmm/object.h>
#include <glibmm/property.h>
#include <iostream>
#include <string>

namespace Tactos::Config {

class TactosColor : public Glib::Object {
public:
  enum SPEECH_TRIGGER { IMMEDIATE, MOUSE_ALL, MOUSE_LEFT, MOUSE_MIDDLE, MIT_1, MIT_2 };

  const static std::string SPEECH_TRIGGER_NAME[];
  static uint16_t color_index;

  static Glib::RefPtr<TactosColor> create(std::string t_name, Gdk::RGBA t_min, Gdk::RGBA t_max, bool t_pins = true,
                                          std::string t_TTS = "", bool t_active = true,
                                          SPEECH_TRIGGER t_speech_trigger = IMMEDIATE, bool t_sound_icon = false,
                                          uint16_t t_index = color_index);
  static Glib::RefPtr<TactosColor> create(std::string t_name, Gdk::RGBA t_min, bool t_pins = true, std::string t_TTS = "",
                                          bool t_active = true, SPEECH_TRIGGER t_speech_trigger = IMMEDIATE,
                                          bool t_sound_icon = false, uint16_t t_index = color_index);

  TactosColor(std::string t_name, Gdk::RGBA t_min, Gdk::RGBA t_max, bool t_pins = true, std::string t_TTS = "",
              bool t_active = true, SPEECH_TRIGGER t_speech_trigger = IMMEDIATE, bool t_sound_icon = false,
              uint16_t t_index = color_index);

  TactosColor(std::string t_name = "", Gdk::RGBA t_color = Gdk::RGBA(), bool t_pins = true, std::string t_TTS = "",
              bool t_active = true, SPEECH_TRIGGER t_speech_trigger = IMMEDIATE, bool t_sound_icon = false,
              uint16_t t_index = color_index);

  TactosColor(TactosColor &&c) noexcept
      : Glib::ObjectBase(typeid(TactosColor)), name(std::move(c.name)), min(std::move(c.min)), max(std::move(c.max)),
        pins(std::move(c.pins)), TTS(std::move(c.TTS)), active(std::move(c.active)), chain(std::move(c.chain)),
        speech_trigger(c.speech_trigger), index(std::move(c.index)), sound_icon(std::move(c.sound_icon)){};

  TactosColor(TactosColor &color) noexcept
      : Glib::ObjectBase(typeid(TactosColor)), name(color.name), min(color.min), max(color.max), pins(color.pins),
        TTS(color.TTS), active(color.active), chain(color.chain), speech_trigger(color.speech_trigger), index(color.index),
        sound_icon(color.sound_icon){};

  bool operator<(const TactosColor &second) { return index < second.index; };
  bool isValid();
  void fixColor();
  sigc::signal<void(Gdk::RGBA, Gdk::RGBA)> sig_color;

  std::string name;
  Gdk::RGBA min;
  Gdk::RGBA max;
  bool pins;
  std::string TTS;
  bool active;
  bool chain;
  SPEECH_TRIGGER speech_trigger;
  uint16_t index;
  bool sound_icon;
};

class ColorListStore : public Gio::ListStore<TactosColor> {
public:
  ColorListStore() : Gio::ListStore<TactosColor>(){};
  static Glib::RefPtr<ColorListStore> create();
  void remove_item(const TactosColor &color);
  std::vector<Glib::RefPtr<TactosColor>> getColors();
  void setColors(std::vector<Glib::RefPtr<TactosColor>> colors);
  void undo();

private:
  Glib::RefPtr<TactosColor> lastDel;
};
} // namespace Tactos::Config
