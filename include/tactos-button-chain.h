#pragma once
#include <gtkmm/checkbutton.h>
#include <gtkmm/icontheme.h>
#include <gtkmm/image.h>

namespace Tactos::Config {

class ChainButton : public Gtk::CheckButton {
public:
  ChainButton(bool chain);

private:
  void toggle_icon();
  Gtk::Image chain_close;
  Gtk::Image chain_open;
};

} // namespace Tactos::Config
