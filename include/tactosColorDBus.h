#pragma once
#include "tactosColor.h"
#include "tactosColor_proxy.h"
#include <glibmm/refptr.h>
#include <optional>

namespace Tactos::Config {

class TactosColorDBus {
public:
  TactosColorDBus();
  bool connect();
  bool isConnected();
  bool tryConnect();
  std::optional<std::vector<Glib::RefPtr<TactosColor>>> getColors();
  bool setColors(std::vector<Glib::RefPtr<TactosColor>> colors);
  std::pair<guint, guint> getWinSize();
  bool setWinSize(std::pair<guint, guint> winSize);

private:
  bool connected;
  std::optional<Glib::RefPtr<org::tactos::colorProxy>> proxy;
};
} // namespace Tactos::Config
