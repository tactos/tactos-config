#pragma once
#include "giomm.h"
#include "glibmm.h"
#include <iostream>
#include <vector>

namespace org {
namespace tactos {

class colorTypeWrap {
public:
  template <typename T> static void unwrapList(std::vector<T> &list, const Glib::VariantContainerBase &wrapped) {
    for (uint i = 0; i < wrapped.get_n_children(); i++) {
      Glib::Variant<T> item;
      wrapped.get_child(item, i);
      list.push_back(item.get());
    }
  }

  static std::vector<Glib::ustring> stdStringVecToGlibStringVec(const std::vector<std::string> &strv) {
    std::vector<Glib::ustring> newStrv;
    for (uint i = 0; i < strv.size(); i++) {
      newStrv.push_back(strv[i]);
    }

    return newStrv;
  }

  static std::vector<std::string> glibStringVecToStdStringVec(const std::vector<Glib::ustring> &strv) {
    std::vector<std::string> newStrv;
    for (uint i = 0; i < strv.size(); i++) {
      newStrv.push_back(strv[i]);
    }

    return newStrv;
  }

  static Glib::VariantContainerBase addColor_pack(guint16 arg_id, const std::tuple<guint32, guint32> &arg_color,
                                                  bool arg_pins, guchar arg_speech_trigger, const Glib::ustring &arg_speech,
                                                  const Glib::ustring &arg_name, bool arg_sound_icon) {
    Glib::VariantContainerBase base;
    std::vector<Glib::VariantBase> params;

    Glib::Variant<guint16> id_param = Glib::Variant<guint16>::create(arg_id);
    params.push_back(id_param);

    Glib::Variant<std::tuple<guint32, guint32>> color_param = Glib::Variant<std::tuple<guint32, guint32>>::create(arg_color);
    params.push_back(color_param);

    Glib::Variant<bool> pins_param = Glib::Variant<bool>::create(arg_pins);
    params.push_back(pins_param);

    Glib::Variant<guchar> speech_trigger_param = Glib::Variant<guchar>::create(arg_speech_trigger);
    params.push_back(speech_trigger_param);

    Glib::Variant<Glib::ustring> speech_param = Glib::Variant<Glib::ustring>::create(arg_speech);
    params.push_back(speech_param);

    Glib::Variant<Glib::ustring> name_param = Glib::Variant<Glib::ustring>::create(arg_name);
    params.push_back(name_param);

    Glib::Variant<bool> sound_icon_param = Glib::Variant<bool>::create(arg_sound_icon);
    params.push_back(sound_icon_param);
    return Glib::VariantContainerBase::create_tuple(params);
  }

  static Glib::VariantContainerBase updateColor_pack(guint16 arg_id, const std::tuple<guint32, guint32> &arg_color,
                                                     bool arg_pins, guchar arg_speech_trigger, const Glib::ustring &arg_name,
                                                     const Glib::ustring &arg_speech, bool arg_sound_icon) {
    Glib::VariantContainerBase base;
    std::vector<Glib::VariantBase> params;

    Glib::Variant<guint16> id_param = Glib::Variant<guint16>::create(arg_id);
    params.push_back(id_param);

    Glib::Variant<std::tuple<guint32, guint32>> color_param = Glib::Variant<std::tuple<guint32, guint32>>::create(arg_color);
    params.push_back(color_param);

    Glib::Variant<bool> pins_param = Glib::Variant<bool>::create(arg_pins);
    params.push_back(pins_param);

    Glib::Variant<guchar> speech_trigger_param = Glib::Variant<guchar>::create(arg_speech_trigger);
    params.push_back(speech_trigger_param);

    Glib::Variant<Glib::ustring> name_param = Glib::Variant<Glib::ustring>::create(arg_name);
    params.push_back(name_param);

    Glib::Variant<Glib::ustring> speech_param = Glib::Variant<Glib::ustring>::create(arg_speech);
    params.push_back(speech_param);

    Glib::Variant<bool> sound_icon_param = Glib::Variant<bool>::create(arg_sound_icon);
    params.push_back(sound_icon_param);
    return Glib::VariantContainerBase::create_tuple(params);
  }

  static Glib::VariantContainerBase removeColor_pack(const std::tuple<guint32, guint32> &arg_color) {
    Glib::VariantContainerBase base;
    Glib::Variant<std::tuple<guint32, guint32>> params = Glib::Variant<std::tuple<guint32, guint32>>::create(arg_color);
    return Glib::VariantContainerBase::create_tuple(params);
  }

  static Glib::VariantContainerBase loadConfigFile_pack(const Glib::ustring &arg_file) {
    Glib::VariantContainerBase base;
    Glib::Variant<Glib::ustring> params = Glib::Variant<Glib::ustring>::create(arg_file);
    return Glib::VariantContainerBase::create_tuple(params);
  }

  static Glib::VariantContainerBase loadConfigList_pack(
      const std::vector<std::tuple<guint16, std::tuple<guint32, guint32>, bool, guchar, Glib::ustring, Glib::ustring, bool>>
          &arg_list) {
    Glib::VariantContainerBase base;
    Glib::Variant<
        std::vector<std::tuple<guint16, std::tuple<guint32, guint32>, bool, guchar, Glib::ustring, Glib::ustring, bool>>>
        params = Glib::Variant<std::vector<std::tuple<guint16, std::tuple<guint32, guint32>, bool, guchar, Glib::ustring,
                                                      Glib::ustring, bool>>>::create(arg_list);
    return Glib::VariantContainerBase::create_tuple(params);
  }
};

} // namespace tactos
} // namespace org
