#pragma once
#include "tactosColor_common.h"
#include <giomm.h>
#include <glibmm.h>
#include <string>
#include <tuple>
#include <vector>

namespace org {
namespace tactos {

class colorProxy : public Glib::ObjectBase {
public:
  static void createForBus(Gio::DBus::BusType busType, Gio::DBus::ProxyFlags proxyFlags, const std::string &name,
                           const std::string &objectPath, const Gio::SlotAsyncReady &slot,
                           const Glib::RefPtr<Gio::Cancellable> &cancellable = {});

  static Glib::RefPtr<colorProxy> createForBusFinish(const Glib::RefPtr<Gio::AsyncResult> &result);

  static Glib::RefPtr<colorProxy> createForBus_sync(Gio::DBus::BusType busType, Gio::DBus::ProxyFlags proxyFlags,
                                                    const std::string &name, const std::string &objectPath,
                                                    const Glib::RefPtr<Gio::Cancellable> &cancellable = {});

  Glib::RefPtr<Gio::DBus::Proxy> dbusProxy() const { return m_proxy; }

  void addColor(guint16 id, const std::tuple<guint32, guint32> &color, bool pins, guchar speech_trigger,
                const Glib::ustring &speech, const Glib::ustring &name, bool sound_icon, const Gio::SlotAsyncReady &slot,
                const Glib::RefPtr<Gio::Cancellable> &cancellable = {}, int timeout_msec = -1);

  void addColor_finish(bool &success, const Glib::RefPtr<Gio::AsyncResult> &res);

  bool addColor_sync(guint16 id, const std::tuple<guint32, guint32> &color, bool pins, guchar speech_trigger,
                     const Glib::ustring &speech, const Glib::ustring &name, bool sound_icon,
                     const Glib::RefPtr<Gio::Cancellable> &cancellable = {}, int timeout_msec = -1);

  void updateColor(guint16 id, const std::tuple<guint32, guint32> &color, bool pins, guchar speech_trigger,
                   const Glib::ustring &name, const Glib::ustring &speech, bool sound_icon, const Gio::SlotAsyncReady &slot,
                   const Glib::RefPtr<Gio::Cancellable> &cancellable = {}, int timeout_msec = -1);

  void updateColor_finish(bool &success, const Glib::RefPtr<Gio::AsyncResult> &res);

  bool updateColor_sync(guint16 id, const std::tuple<guint32, guint32> &color, bool pins, guchar speech_trigger,
                        const Glib::ustring &name, const Glib::ustring &speech, bool sound_icon,
                        const Glib::RefPtr<Gio::Cancellable> &cancellable = {}, int timeout_msec = -1);

  void removeColor(const std::tuple<guint32, guint32> &color, const Gio::SlotAsyncReady &slot,
                   const Glib::RefPtr<Gio::Cancellable> &cancellable = {}, int timeout_msec = -1);

  void removeColor_finish(bool &success, const Glib::RefPtr<Gio::AsyncResult> &res);

  bool removeColor_sync(const std::tuple<guint32, guint32> &color, const Glib::RefPtr<Gio::Cancellable> &cancellable = {},
                        int timeout_msec = -1);

  void loadConfigFile(const Glib::ustring &file, const Gio::SlotAsyncReady &slot,
                      const Glib::RefPtr<Gio::Cancellable> &cancellable = {}, int timeout_msec = -1);

  void loadConfigFile_finish(bool &success, const Glib::RefPtr<Gio::AsyncResult> &res);

  bool loadConfigFile_sync(const Glib::ustring &file, const Glib::RefPtr<Gio::Cancellable> &cancellable = {},
                           int timeout_msec = -1);

  void loadConfigList(
      const std::vector<std::tuple<guint16, std::tuple<guint32, guint32>, bool, guchar, Glib::ustring, Glib::ustring, bool>>
          &list,
      const Gio::SlotAsyncReady &slot, const Glib::RefPtr<Gio::Cancellable> &cancellable = {}, int timeout_msec = -1);

  void loadConfigList_finish(const Glib::RefPtr<Gio::AsyncResult> &res);

  void loadConfigList_sync(
      const std::vector<std::tuple<guint16, std::tuple<guint32, guint32>, bool, guchar, Glib::ustring, Glib::ustring, bool>>
          &list,
      const Glib::RefPtr<Gio::Cancellable> &cancellable = {}, int timeout_msec = -1);

  void getColors(const Gio::SlotAsyncReady &slot, const Glib::RefPtr<Gio::Cancellable> &cancellable = {},
                 int timeout_msec = -1);

  void getColors_finish(
      std::vector<std::tuple<guint16, std::tuple<guint32, guint32>, bool, guchar, Glib::ustring, Glib::ustring, bool>> &list,
      const Glib::RefPtr<Gio::AsyncResult> &res);

  std::vector<std::tuple<guint16, std::tuple<guint32, guint32>, bool, guchar, Glib::ustring, Glib::ustring, bool>>
  getColors_sync(const Glib::RefPtr<Gio::Cancellable> &cancellable = {}, int timeout_msec = -1);

  std::tuple<guchar, guchar> detectionWin_get(bool *ok = nullptr);
  void detectionWin_set(const std::tuple<guchar, guchar> &, const Gio::SlotAsyncReady &);
  void detectionWin_set_finish(const Glib::RefPtr<Gio::AsyncResult> &);
  void detectionWin_set_sync(const std::tuple<guchar, guchar> &);
  sigc::signal<void> &detectionWin_changed() { return m_detectionWin_changed; }

  void reference() const override {}
  void unreference() const override {}

protected:
  Glib::RefPtr<Gio::DBus::Proxy> m_proxy;

private:
  colorProxy(const Glib::RefPtr<Gio::DBus::Proxy> &proxy);

  void handle_signal(const Glib::ustring &sender_name, const Glib::ustring &signal_name,
                     const Glib::VariantContainerBase &parameters);

  void handle_properties_changed(const Gio::DBus::Proxy::MapChangedProperties &changed_properties,
                                 const std::vector<Glib::ustring> &invalidated_properties);

  sigc::signal<void> m_detectionWin_changed;
};

} // namespace tactos
} // namespace org
