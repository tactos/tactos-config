# TACTOS-CONFIG


Tactos-Config is a gui to configure tactos core software and create configurations for specific graphics. This module is being developped by the [University of Technology of Compiegne (UTC)](https://www.utc.fr/)

This  software is designed to work with [tactos-core-color](https://gitlab.com/tactos/tactos-core-color)  software. 

## Building

### Dependecies

- meson (build system)
- gtkmm-3.0 


## Compilation

 Tactos-MIT4 uses [meson](https://mesonbuild.com/) as build system.

```bash
meson --buildtype=release builddir 
cd builddir
ninja install
cd Tactos 
./tactos-config
``

## Contact

This project is developed by the the laboratory of [Costech](http://www.costech.utc.fr/) in the [University of Technology of Compiegne (UTC)](https://www.utc.fr/) in partnership with [hypra](http://hypra.fr/-Accueil-1-.html) and [natbraille](http://natbraille.free.fr/). First version has been developped by Clément Ferry.
